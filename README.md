# qanary-temporary-solution

Docker-compose file to run [Qanary QA Pipeline](https://github.com/WDAqua/Qanary-question-answering-components).

## Why?
The Build & Run instructions in the original repository lead to an error described [here](https://github.com/WDAqua/Qanary-question-answering-components/issues/127).
This is a temporary solution.

## How?
- Follow the first 3 steps from the (instructions)[https://github.com/WDAqua/Qanary-question-answering-components#build-and-run-a-minimal-set-of-components] of the original repo 
- Instead of step 4, place this [docker-compose.yaml](https://gitlab.com/YanaPalacheva/qanary-temporary-solution/-/blob/60778f7bdc7528791ed95fba575fcc3e84c94681/docker-compose.yaml) into *Qanary-question-answering-components* folder and then run **docker-compose up** in the terminal.

Now, the web interface on http://localhost:8080/startquestionansweringwithtextquestion should show the NED-DBpediaSpotlight component in the component list. 
The database is still empty though, it's TBD at the moment.
